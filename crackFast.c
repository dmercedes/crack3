#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char pass[PASS_LEN];
    char hash[128];

};

typedef struct entry entry;
 
// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{

    FILE *in = fopen(filename, "r");
    if (!in)
    {
        perror("Can't open file");
        exit(1);
    }
    
    int arrayLength = 10; 
    entry *entries = malloc(arrayLength * sizeof(entry));
    int count = 0;

    char line[PASS_LEN];
    char *hash;
    char password[PASS_LEN];
    while(fgets(line, PASS_LEN, in) != NULL)
    {
         if (count == arrayLength)
         {
             // Array is full. Make it bigger.
             arrayLength += 10;
             printf("Reallocating space for %d entries\n", arrayLength);
             entries = realloc(entries, arrayLength * sizeof(entry));
         }
       
        char *nl = strchr(line, '\n');
        if(nl) *nl = '\0';
        strcpy(hash, md5(line, strlen(line)));
        scanf(line,"%s",password);
		//printf("%s %s\n", found, hash);
        //fprintf(target,"Hash: %s\n",hash );
        strcpy(entries[count].hash,hash);
        strcpy(entries[count].pass,password);
        count++;
        
    }
    free(hash);
    fclose(in);
    *size = arrayLength;
    return entries;
    exit(1);
}

int comp(const void *t, const void *elem)
{
    int *tt = (int *)t;
    int *eelem = (int *)elem;
    
    if (*tt == *eelem) return 0;
    else if (*tt < *eelem) return -1;
    else return 1;
}
int byHash(const void *a, const void *b)
{
    entry *aa = (entry *)a;
    entry *bb = (entry *)b;
    return strcmp(aa->hash, bb->hash);
}

void printEntry(entry e)
{
    printf("%s, %s\n", e.pass, e.hash);
}
int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    entry *dict = read_dictionary(argv[1], (int*)(sizeof(dict)));
    int length = 0;
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.`
    qsort(dict, length, sizeof(entry), byHash);
    
    // TODO
    // Open the hash file for reading.
    FILE *in = fopen(argv[2], "r");

    // TODO
    // For each hash, search for it in the dictionary using
    char target[128];
    printf("Enter hash:\n");
    scanf("%s", target);
    //compares target and file together
    entry *found = bsearch(target, in, length, sizeof(entry), comp);
    if (found)
    {
        printEntry(*found);
    }
    else{
        printf("Sorry couldnt locate\n");
    }

}